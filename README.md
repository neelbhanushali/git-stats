# Prerequisites
* Date should be in format (YYYY-MM-DD)
* email of the commit author should be correctly configured before commiting

# Dependencies
* Python (2/3) and respective pip. Check [this](https://realpython.com/installing-python/) for installation.
* `click`. to install run `pip install click`. give `sudo` if required.

# Steps to get Git Stats
* Go to the git repository directory
* Download [this](https://gitlab.com/neelbhanushali/git-stats/-/raw/master/git-stats.py) file to current directory
* `python git-stats.py`